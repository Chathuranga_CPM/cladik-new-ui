import React from 'react';
import { Helmet } from 'react-helmet';
import { Hidden } from '@material-ui/core';
import MobileBottomNavigation from '../../components/MobileBottomNavigation';
import MobileHeader from '../../components/MobileHeader';

import StorePageHeader from '../../components/StorePage/Header/Loadable';
import ProductFilters from '../../components/StorePage/ProductFilters/Loadable';
import ProductWelcomeCard from '../../components/StorePage/ProductWelcomeCard/Loadable';
import LatestProducts from '../../components/StorePage/LatestProducts/Loadable';
import FilterItems from '../../components/StorePage/ProductFilters/FilterItems';
import NavigationLinks from '../../components/StorePage/NavigationLinks/Loadable';

import DesktopMainMenu from '../../components/DesktopMainMenu';

import './styles/storepage.min.css';

export default function StorePage() {
  return (
    <div className="storemainwrap">
      <Helmet titleTemplate="%s - Clenup Project" defaultTitle="Clenup Project">
        <meta name="description" content="A Clenup Project application" />
      </Helmet>

      <Hidden only={['lg', 'xl', 'md']}>
        <MobileHeader />
      </Hidden>

      <Hidden only={['sm', 'xs']}>
        <DesktopMainMenu />
      </Hidden>

      {/* Store header */}
      <StorePageHeader />

      <div className="bottomdata">
        <div className="leftcol">
          <NavigationLinks />

          <ProductFilters />

          <Hidden only={['sm', 'xs']}>
            <FilterItems />
          </Hidden>
        </div>

        <div className="rightcol">
          {/* <ProductWelcomeCard/> */}
          <LatestProducts />
        </div>
      </div>

      <Hidden only={['lg', 'xl', 'md']}>
        <div className="mobheightset" />
        <MobileHeader />
        <MobileBottomNavigation />
      </Hidden>
    </div>
  );
}
