/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { withWidth, Hidden, IconButton, Menu } from '@material-ui/core';

// Mobile bottom navigation
import MobileBottomNavigation from '../../components/MobileBottomNavigation';
import DesktopMainMenu from '../../components/DesktopMainMenu';
import AddProductFabButton from '../../components/AddProductFabButton';

import MobileHeader from '../../components/MobileHeader';
import DesktopHeader from '../../components/DesktopHeader';

import LatestProductsList from '../../components/LatestProductsList';
import LatestShopWall from '../../components/LatestShopWall';

import WelcomeMessege from '../../components/WallComponents/WelcomeMessage';
import SinglePost from '../../components/WallComponents/SinglePost';

import './styles/homepage.min.css';

function HomePage(props) {
  const { width } = props;
  const getScreenSize = width;
  let navigationBar;
  let fabButton;
  let mainHeader;

  if (getScreenSize == 'xs' || getScreenSize == 'sm') {
    navigationBar = <MobileBottomNavigation />;
    fabButton = <AddProductFabButton />;
    mainHeader = <MobileHeader />;
  } else {
    mainHeader = <DesktopHeader />;
    navigationBar = <DesktopMainMenu />;
  }

  return (
    <div>
      <Hidden only={['lg']}>
        <div className="mobheightset" />
      </Hidden>

      <div className="wrapmidddlecon">
        <div className="middlepostarea">
          <Hidden only={['sm', 'xs', 'md']}>
            <div className="mobheightset" />
          </Hidden>
          {mainHeader}
          <WelcomeMessege />
          <SinglePost />
          <SinglePost />
          <SinglePost />
        </div>

        <Hidden only={['sm', 'xs', 'md']}>
          <div className="rightdatasets">
            <div className="stickybar">
              <LatestProductsList />
              <LatestShopWall />
            </div>
          </div>
        </Hidden>
      </div>

      <div className="mobheightset" />

      {/* Fab icon only for mobile */}
      {fabButton}

      {/* Main navigation */}
      {navigationBar}
    </div>
  );
}

HomePage.propTypes = {
  width: PropTypes.oneOf(['lg', 'md', 'sm', 'xl', 'xs']).isRequired,
};

export default withWidth()(HomePage);
