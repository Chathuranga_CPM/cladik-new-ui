/*
 * UserRegister
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import './style/userregister.min.css';
import { Grid, TextField, Button } from '@material-ui/core';

export default function UserRegister() {
	return (
		<div className="userregisterfromwrap">
			<Helmet>
				<title>UserRegister </title>
				<meta
					name="description"
					content="UserRegister of React.js Boilerplate application"
				/>
			</Helmet>
			<div className="wrapmainfrom">
				<div className="formarea">
					<h1>User Registation</h1>
					<form noValidate autoComplete="off">
						<TextField id="outlined-basic" label="First Name" variant="outlined" fullWidth/>
						<TextField id="outlined-basic" label="Last Name" variant="outlined" fullWidth/>
						<TextField id="outlined-basic" label="Email address" variant="outlined" fullWidth/>
						<TextField id="outlined-basic" label="Contact number" variant="outlined" fullWidth/>
						<TextField id="outlined-basic" label="Password" variant="outlined" fullWidth/>
						<TextField id="outlined-basic" label="Confirm Password" variant="outlined" fullWidth/>
						<Button variant="contained" color="primary">
							Create account
						</Button>
					</form>
				</div>
				<div className="rightdata">
					Right data
				</div>
			</div>

			
		</div>
	);
}
