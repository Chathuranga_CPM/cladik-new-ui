/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import { Switch, Route } from 'react-router-dom';

import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import HomePage from 'containers/HomePage/Loadable';
import FeaturePage from 'containers/FeaturePage/Loadable';
import RegisterPage from 'containers/Register/Loadable';
import SingleProduct from 'containers/SingleProduct/Loadable';
import StorePage from 'containers/StorePage/Loadable';
import UserRegister from 'containers/UserRegister/Loadable';
import UserLogin from 'containers/UserLogin/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Footer from 'components/Footer';

import { createMuiTheme } from '@material-ui/core/styles';

import GlobalStyle from '../../global-styles';

const theme = createMuiTheme({
  palette: {
    primary: indigo,
    secondary: {
      light: '#ff7961',
      main: '#2979ff',
      dark: '#ba000d',
      // contrastText: '#000',
    },
    // type: 'dark',
  },
  shadows: ['none'],

  overrides: {
    MuiButton: {
      root: {
        backgroundColor: indigo,
      },
      contained: {
        // boxShadow:"0 0 0",
        // backgroundColor: '#000',
      },
      label: {
        textTransform: 'none',
      },
    },
  },
});

export default function App() {
  return (
    <div>
      <Helmet titleTemplate="%s - Clenup Project" defaultTitle="Clenup Project">
        <meta name="description" content="A Clenup Project application" />
      </Helmet>
      <MuiThemeProvider theme={theme}>
        {/* <Header /> */}
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/features" component={FeaturePage} />
          <Route path="/store-page" component={StorePage} />
          <Route path="/register" component={RegisterPage} />
          <Route path="/user-register" component={UserRegister} />
          <Route path="/user-login" component={UserLogin} />
          <Route path="/single-product" component={SingleProduct} />
          <Route path="/single-store" component={StorePage} />
          <Route path="" component={NotFoundPage} />
        </Switch>
        <Footer />
      </MuiThemeProvider>
      <GlobalStyle />
    </div>
  );
}
