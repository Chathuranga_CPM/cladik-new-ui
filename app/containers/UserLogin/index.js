/*
 * UserLogin
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import './style/userlogin.min.css';
import { Grid, TextField, Button } from '@material-ui/core';

export default function UserLogin() {
	return (
		<div className="userloginfromwrap">
			<Helmet>
				<title>UserLogin </title>
				<meta
					name="description"
					content="UserLogin of React.js Boilerplate application"
				/>
			</Helmet>
			<div className="wrapmainfrom">
				<div className="formarea">
					<h1>User Login</h1>
					<form noValidate autoComplete="off">
						<TextField id="outlined-basic" label="Email address" variant="outlined" fullWidth/>
						<TextField id="outlined-basic" label="Password" variant="outlined" fullWidth/>
						<Button variant="contained" color="primary">
							Login
						</Button>
					</form>
				</div>
				<div className="rightdata">
					Right data
				</div>
			</div>

			
		</div>
	);
}
