/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';

import Button from '@material-ui/core/Button';


export default function FeaturePage() {
  return (
    <div>
      Feature Page
      <Helmet>
        <title>Feature Page </title>
        <meta
          name="description"
          content="Feature page of React.js Boilerplate application"
        />
      </Helmet>
      <Button variant="contained" color="primary">
        Hello World
      </Button>
    </div>
  );
}
