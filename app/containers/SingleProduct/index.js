import React from 'react';
import { Helmet } from 'react-helmet';
import MobileBottomNavigation from '../../components/MobileBottomNavigation';
import MobileTopNav from '../../components/SingleProduct/MobileTopNav';

import ProductImageCarousel from '../../components/SingleProduct/ProductImageCarousel/Loadable';
import ProductFeatureDetails from '../../components/SingleProduct/ProductFeatureDetails/Loadable';

import DesktopMainMenu from '../../components/DesktopMainMenu';

import './styles/sinproductmainwrapper.min.css';

import { Hidden } from '@material-ui/core';

export default function SingleProduct() {
  return (
    <div className="sinproductmainwrapper">
      <Helmet titleTemplate="%s - Clenup Project" defaultTitle="Clenup Project">
        <meta name="description" content="A Clenup Project application" />
      </Helmet>

      <Hidden only={['sm', 'xs']}>
        <DesktopMainMenu />
      </Hidden>

      <div className="leftsidedata">
        <ProductImageCarousel />
      </div>
      <div className="rightsidedata">
        <ProductFeatureDetails />
      </div>

      <Hidden only={['lg', 'xl', 'md']}>
        {/* <div className="mobheightset"/> */}
        <MobileTopNav />
        {/* <MobileBottomNavigation /> */}
      </Hidden>
      {/* Single product page */}
    </div>
  );
}
