import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
*,
*::before,
*::after {
  box-sizing: border-box;
}

html {
  // cursor: none;
  line-height: 1.15;
  -webkit-text-size-adjust: 100%;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  scroll-behavior: smooth;
}

article,
aside,
figcaption,
figure,
footer,
header,
hgroup,
main,
nav,
section {
  display: block;
}

body {
  margin: 0;
  // font-family: "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
  font-family: 'Poppins', sans-serif;
  font-weight: 400;
  line-height: 1.5;
  background-color: rgba(0,0,0,0.04);
}

[tabindex='-1']:focus {
  outline: 0 !important;
}

hr {
  box-sizing: content-box;
  height: 0;
  overflow: visible;
}

h1,
h2,
h3,
h4,
h5,
h6 {
  margin-top: 0;
}

p {
  margin-top: 0;
}

abbr[title],
abbr[data-original-title] {
  text-decoration: underline;
  -webkit-text-decoration: underline dotted;
  text-decoration: underline dotted;
  cursor: help;
  border-bottom: 0;
  -webkit-text-decoration-skip-ink: none;
  text-decoration-skip-ink: none;
}

address {
  margin-bottom: 1rem;
  font-style: normal;
  line-height: inherit;
}

ol,
ul,
dl {
  margin-top: 0;
}

ol ol,
ul ul,
ol ul,
ul ol {
  margin-bottom: 0;
}

dt {
  font-weight: 700;
}

dd {
  margin-bottom: 0.5rem;
  margin-left: 0;
}

blockquote {
  margin: 0 0 1rem;
}

b,
strong {
  font-weight: bolder;
}

small {
  font-size: 80%;
}

sub,
sup {
  position: relative;
  font-size: 75%;
  line-height: 0;
  vertical-align: baseline;
}

sub {
  bottom: -0.25em;
}

sup {
  top: -0.5em;
}

a:hover {
  color: #0056b3;
}

a:not([href]):not([tabindex]) {
  color: inherit;
  text-decoration: none;
}

a:not([href]):not([tabindex]):hover,
a:not([href]):not([tabindex]):focus {
  color: inherit;
  text-decoration: none;
}

a:not([href]):not([tabindex]):focus {
  outline: 0;
}

pre,
code,
kbd,
samp {
  font-family: SFMono-Regular, Menlo, Monaco, Consolas, 'Liberation Mono',
    'Courier New', monospace;
  font-size: 1em;
}

pre {
  margin-top: 0;
  margin-bottom: 1rem;
  overflow: auto;
}

figure {
  margin: 0 0 1rem;
}

img {
  vertical-align: middle;
  border-style: none;
}

svg {
  overflow: hidden;
  vertical-align: middle;
}

table {
  border-collapse: collapse;
}

caption {
  padding-top: 0.75rem;
  padding-bottom: 0.75rem;
  color: #6c757d;
  text-align: left;
  caption-side: bottom;
}

th {
  text-align: inherit;
}



fieldset {
  min-width: 0;
  padding: 0;
  margin: 0;
  border: 0;
}

legend {
  display: block;
  width: 100%;
  max-width: 100%;
  padding: 0;
  margin-bottom: 0.5rem;
  font-size: 1.5rem;
  line-height: inherit;
  color: inherit;
  white-space: normal;
}

ul{
  padding:0;
  margin:0;
}

.background {
  height: 100%;
  left: 0;
  object-fit: cover;
  top: 0;
  transform: none;
  width: 100%;
  position: absolute;
  z-index: -1;
}

img {
  width: 100%;
  display: block;
}

.clearfix::after {
  display: block;
  clear: both;
  content: '';
}

.mobheightset{
  height:65px;
}

a {
  &.deflinkbtn {
    background: #0057b338;
    color: #0056b3;
    display: block;
    padding: 7px 15px;
    border-radius: 5px;
    font-size: 14px;
    font-weight: bold;
    text-decoration: none;
  }
}

.notfound{
  text-align:center;
  margin-top:100px;
  img{
    width: 200px;
    margin: 40px auto;
  }
  p{
    color:rgba(0,0,0,0.7);
    font-size:15px;
  }
}

button{
  &.rounded {
    border-radius:30px;
    padding-left:11px;
    .MuiButton-startIcon{
      width: 35px;
      height: 35px;
      border-radius:100%;
      background:rgba(255,255,255,0.2);
      padding:5px;
      position:relative;
      svg{
          position:absolute;
          top:50%;
          left:50%;
          transform:translate(-50%, -50%);
          fill: #fff;
          width:20px;
          height:20px;
      }
  }
  }
}

.removepadding{
  .MuiList-padding{
    padding:0;
  }
  .MuiPopover-paper{
    border-radius:20px
  }
}
.nowrap{
  
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}

.borderset{
  border: 1px solid rgba(0,0,0,0.1);
  box-shadow: 2px 1px 0px 0px rgba(0,0,0,0.02);
}

@media (min-width: 1024px){
  .mobheightset{
    height:120px;
  }
}

@media (min-width: 1200px) {
  .mobheightset{
    height:8vw;
  }
  button {
    &.rounded {
      border-radius: 3vw;
      height: 2.7vw;
      padding-left: .5vw;
      padding: .5vw 1vw .5vw .8vw;
      font-size: .8vw;

      .MuiButton-startIcon {
          width: 1.7vw;
          height: 1.7vw;
          padding: .6vw;
          border-radius: 3vw;
          position: relative;
          svg {
              width: 1vw;
              height: 1vw;
          }
      }

    }
  }
  .removepadding{
    .MuiPopover-paper{
      border-radius:1vw
    }
  }
  .notfound{
    margin-top:10vw;
    img{
      width: 20vw;
      margin: 5vw auto;
    }
    h1{
      margin-bottom:2vw;
    }
    p{
      font-size:.95vw;
    }
  } 
}
`;

export default GlobalStyle;
