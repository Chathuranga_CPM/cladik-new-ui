import React from 'react';
import { Link } from 'react-router-dom';
import { IconButton, Button } from '@material-ui/core';
import IconBack from '../../Icons/Svg/IconBack';
import IconCheckMark from '../../Icons/Svg/IconCheckMark';

import './styles/shippingcalculator.min.css';

import box from '../../../images/svg/box.svg';

import { makeStyles, fade, createMuiTheme } from '@material-ui/core/styles';
const theme = createMuiTheme();

const useStyles = makeStyles(theme => ({
  setthemecolor: {
    color: `${fade(theme.palette.primary.main, 1)}`,
    backgroundColor: `${fade(theme.palette.primary.main, 0.1)}`,
    '&:focus, &:hover': {
      backgroundColor: `${fade(theme.palette.primary.main, 0.5)}`,
    },
  },
}));

export default function ShippingCalculator() {
  const classes = useStyles();

  return (
    <div className="shippingcalculatorpopwrap">
      <div className="headertitle">
        <div className="backicon">
          <IconButton aria-label="back" size="small">
            <IconBack />
          </IconButton>
        </div>
        <span>Shipping Method</span>
      </div>
      <div className="scrollerarea">
        <div className="shippingto">
          <span className="toptitle">
            Shipping to <b>160c, Nawala Rd, Nugegoda</b>
          </span>
          <Button
            variant="contained"
            color="primary"
            className={`${classes.setthemecolor} changeaddress`}
          >
            {' '}
            Change Address
          </Button>
        </div>

        <div className="shippingmethod">
          <span className="methodtitle">Shipping method</span>
          <div className="listofshipping">
            <div className="sinrow active">
              <div className="icon">
                <img
                  src="https://lh3.googleusercontent.com/G4nox8fDHizN-0bDS2VsIhCuM0_KTZ1TBpfrbse7gt7ryhhSKtHuKfrLT1WXLCLPa5gV"
                  className="background"
                />
              </div>
              <div className="dataarea">
                <b>Shipping : LKR 100</b>
                <span>Vai SL Post EMS</span>
                <span>Estimate Delivery: 7-10 days</span>
              </div>
              <div className="checkmark">
                <IconCheckMark />
              </div>
            </div>
            <div className="sinrow">
              <div className="icon">
                <img
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS4XZDHXHEkdb2C_IF7y5dgAVpOeVl_lDyALu51kPoz_IVDx6Og&usqp=CAU"
                  className="background"
                />
              </div>
              <div className="dataarea">
                <b>Shipping : LKR 100</b>
                <span>Vai SL Post eEMS</span>
                <span>Estimate Delivery: 7-10 days</span>
              </div>
              <div className="checkmark">
                <IconCheckMark />
              </div>
            </div>
            <div className="sinrow">
              <div className="icon">
                <img
                  src="https://www.pngitem.com/pimgs/m/168-1684299_content-grasshopper-logo-best-grasshopper-logo-hd-png.png"
                  className="background"
                />
              </div>
              <div className="dataarea">
                <b>Shipping : LKR 100</b>
                <span>Vai SL Post eEMS</span>
                <span>Estimate Delivery: 7-10 days</span>
              </div>
              <div className="checkmark">
                <IconCheckMark />
              </div>
            </div>
            <div className="sinrow">
              <div className="icon">
                <img
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS4XZDHXHEkdb2C_IF7y5dgAVpOeVl_lDyALu51kPoz_IVDx6Og&usqp=CAU"
                  className="background"
                />
              </div>
              <div className="dataarea">
                <b>Shipping : LKR 100</b>
                <span>Vai SL Post eEMS</span>
                <span>Estimate Delivery: 7-10 days</span>
              </div>
              <div className="checkmark">
                <IconCheckMark />
              </div>
            </div>
            <div className="sinrow">
              <div className="icon">
                <img
                  src="https://www.pngitem.com/pimgs/m/168-1684299_content-grasshopper-logo-best-grasshopper-logo-hd-png.png"
                  className="background"
                />
              </div>
              <div className="dataarea">
                <b>Shipping : LKR 100</b>
                <span>Vai SL Post eEMS</span>
                <span>Estimate Delivery: 7-10 days</span>
              </div>
              <div className="checkmark">
                <IconCheckMark />
              </div>
            </div>
            <div className="sinrow">
              <div className="icon">
                <img
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS4XZDHXHEkdb2C_IF7y5dgAVpOeVl_lDyALu51kPoz_IVDx6Og&usqp=CAU"
                  className="background"
                />
              </div>
              <div className="dataarea">
                <b>Shipping : LKR 100</b>
                <span>Vai SL Post eEMS</span>
                <span>Estimate Delivery: 7-10 days</span>
              </div>
              <div className="checkmark">
                <IconCheckMark />
              </div>
            </div>
            <div className="sinrow">
              <div className="icon">
                <img
                  src="https://www.pngitem.com/pimgs/m/168-1684299_content-grasshopper-logo-best-grasshopper-logo-hd-png.png"
                  className="background"
                />
              </div>
              <div className="dataarea">
                <b>Shipping : LKR 100</b>
                <span>Vai SL Post eEMS</span>
                <span>Estimate Delivery: 7-10 days</span>
              </div>
              <div className="checkmark">
                <IconCheckMark />
              </div>
            </div>
          </div>
        </div>
        <div className="packegedata">
          <div className="icon">
            <img src={box} />
          </div>
          <div className="dataset">
            <div className="rowdata">
              <span className="val">0.05kg</span>
              <span>weight</span>
            </div>
            <div className="rowdata">
              <span className="val">20cm * 10cm * 3cm</span>
              <span>size</span>
            </div>
            <div className="rowdata">
              <span className="val">Ship out withing 3 days</span>
              <span>Processing Time</span>
            </div>
          </div>
        </div>
      </div>
      <div className="ftrbtn">
        <Button
          variant="contained"
          color="primary"
          className={`${classes.setthemecolor} applybutton`}
        >
          Apply
        </Button>
      </div>
    </div>
  );
}
