import React from 'react';
import './styles/productimagecarousel.min.css';

export default function ProductImageCarousel() {
  return (
    <div className="sinproimagecarouselwrap">
      <div className="mainimgwrap">
        <img
          src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/shopping-bag-full-of-fresh-vegetables-and-fruits-royalty-free-image-1128687123-1564523576.jpg"
          className="background"
        />
      </div>
    </div>
  );
}
