import React from 'react';
import { Link } from 'react-router-dom';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import {
  Hidden,
  IconButton,
  SwipeableDrawer,
  Menu,
  Drawer,
  TextField,
  Button,
} from '@material-ui/core';
import { red, grey, blue, green } from '@material-ui/core/colors';
import { makeStyles, fade, createMuiTheme } from '@material-ui/core/styles';
import ShippingCalculator from '../ShippingCalculator/Loadable';
import SmallSellerCard from '../../SmallSellerCard/Loadable';
import RatingGraph from '../../RatingGraph';

import IconStore from '../../Icons/Svg/IconStore';
import IconCart from '../../Icons/Svg/IconCart';
import IconBuy from '../../Icons/Svg/IconBuy';

import './styles/productfeaturedetails.min.css';
import IconRight from '../../Icons/Svg/IconRight';

import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';

const theme = createMuiTheme();

const useStyles = makeStyles(theme => ({
  setthemecolor: {
    color: `${fade(theme.palette.primary.main, 1)}`,
  },
  setthemecoloropacity: {
    backgroundColor: `${fade(theme.palette.primary.main, 1)}`,
  },
  setthemebackcolor: {
    backgroundColor: `${fade(theme.palette.primary.main, 0.8)}`,
  },
  fillsvg: {
    fill: `${fade(theme.palette.primary.main, 1)}`,
  },
  red: {
    backgroundColor: red.A700,
  },
  green: {
    backgroundColor: green.A700,
  },
  black: {
    backgroundColor: '#000',
  },
  blue: {
    backgroundColor: blue.A700,
  },
}));

export default function ProductFeatureDetails() {
  const classes = useStyles();
  const [value, setValue] = React.useState(2);
  const [state, setState] = React.useState({
    bottom: false,
  });

  const toggleDrawer = (anchor, open) => event => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div className="sinprofeatdetailswrap">
      <div className="topheader">
        <h1>
          10pcs DC12V LED Light Strip COB Lamp 60CM 50CM 40CM 30CM 20CM 12V LED
          Bar Lights Warm Cool Day White Bulbs for DIY Lighting
        </h1>
        <div className="reviewswrap">
          <div className="starwra">
            <span className="val">4.7</span>
            <Box component="fieldset" mb={3} borderColor="transparent">
              <Rating
                name="simple-controlled"
                value={value}
                onChange={(event, newValue) => {
                  setValue(newValue);
                }}
              />
            </Box>
          </div>
          <div className="ordercount">154 Order</div>
        </div>
        <div className="price">
          <span className={`${classes.setthemecolor} reguler`}>
            LKR : 1500.00/=
          </span>
          <span className="sales">
            <del>LKR : 1500.00/=</del>
          </span>
        </div>

        <div className="sinquentity">
          <div className="btnwrap">
            <IconButton aria-label="minus">
              <RemoveIcon />
            </IconButton>
            <TextField id="" label="" value="1" />
            <IconButton aria-label="minus">
              <AddIcon />
            </IconButton>
          </div>
          <div className="avalival">61 pieces available</div>
        </div>

        <div className="shipping" onClick={toggleDrawer('bottom', true)}>
          <div className="lefttitle">
            <span>Shipping</span>
            <span className="detail">
              To <b>Vijerama</b> vai SL Post
            </span>
          </div>
          <div className="rightarrow">
            <IconRight />
          </div>
        </div>

        <div className="buybuttonswrap">
          <div className="mainwrapbuybtn">
            <div className="storepg">
              <IconButton aria-label="store">
                <IconStore className={classes.fillsvg} />
              </IconButton>
            </div>
            <div className="otherbtnwrap">
              <Button
                variant="contained"
                color="primary"
                startIcon={<IconCart />}
                className={`${classes.setthemecoloropacity}`}
              >
                Add To Card
              </Button>
              <Button
                variant="contained"
                color="primary"
                startIcon={<IconBuy />}
                className={`${classes.setthemecoloropacity}`}
              >
                Buy Now
              </Button>
            </div>
          </div>
        </div>

        <SwipeableDrawer
          anchor="bottom"
          open={state.bottom}
          onClose={toggleDrawer('bottom', false)}
          onOpen={toggleDrawer('bottom', true)}
          className="shippingdatapopup"
        >
          <ShippingCalculator />
        </SwipeableDrawer>
      </div>

      <div className="sinmoredatarow">
        <div className="lefttitle">
          <span>Varians, Color</span>
          <span className="detail">Click here for this product veriation</span>
          <div className="colorpackets">
            <span className={classes.red} />
            <span className={classes.green} />
            <span className={classes.blue} />
            <span className={classes.black} />
          </div>
        </div>
        <div className="rightarrow">
          <IconRight />
        </div>
        <div className="secon">
          <div className="lefttitle">
            <span>Specifications</span>
          </div>
          <div className="rightarrow">
            <IconRight />
          </div>
        </div>
      </div>

      <div className="sinmoredatarow ratinglist">
        <div className="lefttitle">
          <span>Customer reviews (450)</span>
          <RatingGraph />
        </div>
        <div className="rightarrow">
          <IconRight />
        </div>
      </div>

      <SmallSellerCard />

      <div className="mobheightset" />
    </div>
  );
}
