import React from 'react';
import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';

import IconCart from '../../Icons/Svg/IconCart';
import IconBack from '../../Icons/Svg/IconBack';
import IconStore from '../../Icons/Svg/IconStore';

import './styles/mobiletopnavpro.min.css';

export default function MobileTopNav() {
  return (
    <div className="sinprotopnavmobwrap">
      <div className="backicon">
        <IconButton aria-label="back" size="small">
          <IconBack />
        </IconButton>
      </div>
      <div className="hiddentitle">
        <span>Product Title goese here</span>
      </div>
      <div className="carticon">
        <IconButton aria-label="store" size="small">
          <IconStore />
        </IconButton>
        <IconButton aria-label="cart" size="small">
          <IconCart />
        </IconButton>
      </div>
    </div>
  );
}
