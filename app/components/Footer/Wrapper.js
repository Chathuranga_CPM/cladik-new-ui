import styled from 'styled-components';

const Wrapper = styled.footer`
  text-align: center;
  color: rgba(0, 0, 0, 0.7);
  font-size: 12px;
  padding-bottom: 20px;
`;

export default Wrapper;
