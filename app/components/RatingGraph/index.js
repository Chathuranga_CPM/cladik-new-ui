import React from 'react';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import './styles/ratinggraph.min.css';

import StarIcon from '@material-ui/icons/Star';

export default function RatingGraph() {
  const [value, setValue] = React.useState(2);
  return (
    <div className="ratinggraphwrap">
      <div className="ratingwrap">
        <div className="value">
          <span className="currentval">4.5</span>
          <Box component="fieldset" mb={3} borderColor="transparent">
            <Rating
              name="simple-controlled"
              value={value}
              onChange={(event, newValue) => {
                setValue(newValue);
              }}
            />
          </Box>
        </div>
        <div className="ratingbars">
          <div className="startlist">
            <div className="sinrow">
              <StarIcon />
              <StarIcon />
              <StarIcon />
              <StarIcon />
              <StarIcon />
            </div>
            <div className="sinrow">
              <StarIcon />
              <StarIcon />
              <StarIcon />
              <StarIcon />
            </div>
            <div className="sinrow">
              <StarIcon />
              <StarIcon />
              <StarIcon />
            </div>
            <div className="sinrow">
              <StarIcon />
              <StarIcon />
            </div>
            <div className="sinrow">
              <StarIcon />
            </div>
          </div>
          <div className="barlist">
            <div className="sinbar">
              <span style={{ width: '90%' }} />
            </div>
            <div className="sinbar">
              <span style={{ width: '80%' }} />
            </div>
            <div className="sinbar">
              <span style={{ width: '40%' }} />
            </div>
            <div className="sinbar">
              <span style={{ width: '20%' }} />
            </div>
            <div className="sinbar">
              <span style={{ width: '5%' }} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
