import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import Skeleton from '@material-ui/lab/Skeleton';
import { TextField, Select, MenuItem, InputLabel,Grid, OutlinedInput, InputAdornment, IconButton, FormControl } from '@material-ui/core';

import './styles/stepbusinessinfor.min.css';

export default function StepBusinessInformations() {


    const [age, setAge] = React.useState('');

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    return (
        <div>
            <Helmet>
                <title>Bussiness Informations</title>
                <meta
                    name="description"
                    content="Bussiness Informations"
                />
            </Helmet>

           
            <div className="cardmiddle">
                <h1>Bussiness Informations</h1>

                <form noValidate autoComplete="off">
                    <div className="sinrow">
                        <TextField id="outlined-basic" label="Business name" variant="outlined" fullWidth />
                    </div>
                    <div className="sinrow">
                        <TextField id="outlined-basic" label="Contact number" variant="outlined" fullWidth />
                    </div>

                    <div className="sinrow">
                        <FormControl variant="outlined" fullWidth> 
                            <InputLabel id="demo-simple-select-outlined-label">Business category</InputLabel>
                            <Select
                                labelId="demo-simple-select-outlined-label"
                                id="demo-simple-select-outlined"
                                value={age}
                                onChange={handleChange}
                                label="Business category"
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                <MenuItem value={10}>Ten</MenuItem>
                                <MenuItem value={20}>Twenty</MenuItem>
                                <MenuItem value={30}>Thirty</MenuItem>
                            </Select>
                        </FormControl>
                    </div>
                  

                </form>
            </div>
        </div>
    );
}