/**
 * Asynchronously loads the component for FeaturePage
 */

import React from 'react';
import loadable from 'utils/loadable';
import Skeleton from '@material-ui/lab/Skeleton';

import './styles/stepbusinessinfor.min.css';

export default loadable(() => import('./index'), {
	fallback: <div className="cardmiddle">
		<Skeleton variant="text" animation="wave" height={40}/><br/>
		<Skeleton variant="rect" animation="wave" width="100%" height={60} /><br/>
		<Skeleton variant="rect" animation="wave" width="100%" height={60} /><br/>
		<Skeleton variant="rect" animation="wave" width="100%" height={60} /><br/>
	</div>,
});
