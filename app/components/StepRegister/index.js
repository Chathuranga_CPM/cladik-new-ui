import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { TextField, InputLabel, OutlinedInput, InputAdornment, IconButton,FormControl  } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import './styles/stepregister.min.css';

export default function StepRegister() {
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
    });

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    return (
        <div>
            <Helmet>
                <title>User Registation</title>
                <meta
                    name="description"
                    content="User Registation"
                />
            </Helmet>

            <div className="cardmiddle">
                <h1>User Registation</h1>

                <form noValidate autoComplete="off">
                    <div className="sinrow">
                        <TextField id="outlined-basic" label="First name" variant="outlined" fullWidth />
                    </div>
                    <div className="sinrow">
                        <TextField id="outlined-basic" label="Last name" variant="outlined" fullWidth />
                    </div>
                    <div className="sinrow">
                        <TextField id="outlined-basic" label="Email address" variant="outlined" fullWidth />
                    </div>
                    <div className="sinrow">
                        <FormControl variant="outlined" fullWidth>
                            <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={values.showPassword ? 'text' : 'password'}
                                value={values.password}
                                onChange={handleChange('password')}
                                endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                    edge="end"
                                    >
                                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                                }
                                labelWidth={70}
                            />
                        </FormControl>
                    </div>
                </form>
            </div>
        </div>
    );
}