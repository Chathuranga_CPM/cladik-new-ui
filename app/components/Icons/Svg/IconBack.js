import React from 'react';
import PropTypes from 'prop-types';

function IconBack(props) {
  return (
    <svg
      version="1.1"
      id="Capa_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 482.239 482.239"
    >
      <path d="m206.812 34.446-206.812 206.673 206.743 206.674 24.353-24.284-165.167-165.167h416.31v-34.445h-416.31l165.236-165.236z" />
    </svg>
  );
}

export default IconBack;
