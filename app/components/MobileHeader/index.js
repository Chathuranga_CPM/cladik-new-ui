import React, { useEffect, memo } from 'react';
import { Avatar, Badge, Button, SwipeableDrawer } from '@material-ui/core';

import SideProfileCard from '../SideProfileCard/Loadable';

import IconSeach from '../Icons/Svg/IconSearch';


import './styles/mobileheader.min.css';


export default function MobileHeader() {


    const [state, setState] = React.useState({
        right: false,
    });

    const toggleDrawer = (anchor, open) => (event) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setState({ ...state, [anchor]: open });
    };
    return (
        <div className="mobileheader">
            <div className="mainlogowrap">
                <a href="">
                    <img src="https://cladik.com/assets/img/cladik-logo2.svg" />
                </a>
            </div>
            <div className="rightdata">
                <div className="searchicon">
                    <Button>
                        <IconSeach />
                    </Button>
                </div>
                <div className="userimg">
                    <Button onClick={toggleDrawer('right', true)}>
                        <Badge badgeContent={4} color="error" overlap="circle" >
                            <Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" />
                        </Badge>
                    </Button>
                </div>
            </div>

            <SwipeableDrawer
                anchor='right'
                open={state['right']}
                onClose={toggleDrawer('right', false)}
                onOpen={toggleDrawer('right', true)}
            >
                <SideProfileCard/>
            </SwipeableDrawer>
        </div>
    );
}