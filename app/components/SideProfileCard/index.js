import React, { useEffect, memo } from 'react';
import { Avatar, Badge } from '@material-ui/core';

import IconCart from '../Icons/Svg/IconCart';
import IconChat from '../Icons/Svg/IconChat';
import IconNotification from '../Icons/Svg/IconNotification';
import IconLogOut from '../Icons/Svg/IconLogOut';
import IconProfile from '../Icons/Svg/IconProfile';

import './styles/sideprofilecard.min.css';


export default function SideProfileCard() {

    return (
        <div className="drawerdatamobile skeleton">
            <div className="userdata">
                <Badge badgeContent={4} color="error" overlap="circle" >
                    <Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" />
                </Badge>
                <h3>Chathuranga CPM</h3>
            </div>

            <div className="linklist">
                <ul>
                    <li>
                        <a href="">
                            <IconProfile />
                            <span>Profile</span>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <IconNotification />
                            <span>Notifications</span>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <IconChat />
                            <span>Messeges</span>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <IconCart />
                            <span>Cart</span>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <IconLogOut />
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
}