import React, { useEffect, memo } from 'react';
import { Button, TextField } from '@material-ui/core';

import AddIcon from '@material-ui/icons/Add';

import IconSeach from '../Icons/Svg/IconSearch';


import './styles/desktopheader.min.css';


export default function DesktopHeader() {
    return (
        <div className="desktopheader">
            <div className="mainsearchinput">
                <div className="inputwrapper">
                    <IconSeach />
                    <input type="text" placeholder="Search something...."/>
                </div>
            </div>
            <div className="rightdata">
                <Button aria-label="add product" variant="contained" color="primary" startIcon={<AddIcon />}>Add Product</Button>
            </div>
        </div>
    );
}