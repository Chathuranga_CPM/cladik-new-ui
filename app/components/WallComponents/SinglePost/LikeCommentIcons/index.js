import React, { useEffect, memo } from 'react';
import { Button, IconButton, TextField } from '@material-ui/core';

import './styles/likecommenticons.min.css';

import IconHeart from '../../../Icons/Svg/IconHeart';
import IconHeartFill from '../../../Icons/Svg/IconHeartFill';
import IconComment from '../../../Icons/Svg/IconComment';
import IconCommentFill from '../../../Icons/Svg/IconCommentFill';



export default function LikeCommentIcons() {
    return (
        <div className="likecomiconswrap">
            <ul>
                <li>
                    <Button color="default">  
                        <IconHeart/>
                        <span>Love it</span>
                    </Button>
                </li>
                <li>
                    <Button color="default">  
                        <IconComment/>
                        <span>Comment</span>
                    </Button>
                </li>
            </ul>
        </div>
    )
}