import React, { useEffect, memo } from 'react';
import { Button, IconButton, TextField } from '@material-ui/core';

import './styles/addtocartbuttons.min.css';

import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import IconCart from '../../../Icons/Svg/IconCart';

export default function AddToCartButtons() {
  return (
    <div className="addtocartbuttonswrap">
      <div className="quntitywrapper">
        <span className="toptitle">Quntity</span>
        <div className="btnwrap">
          <IconButton aria-label="minus">
            <RemoveIcon />
          </IconButton>
          <TextField id="" label="" value="1" />
          <IconButton aria-label="minus">
            <AddIcon />
          </IconButton>
        </div>
      </div>
      <div className="rightbtnwrapper">
        <Button
          variant="contained"
          color="primary"
          className="rounded"
          startIcon={<IconCart />}
        >
          Add to bucket
        </Button>
      </div>
    </div>
  );
}
