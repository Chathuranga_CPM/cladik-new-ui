import React, { useEffect, memo } from 'react';
import { IconButton} from '@material-ui/core';
import './styles/opstoptionmenu.min.css';

import IconBag from '../../../Icons/Svg/IconBag';
import IconStore from '../../../Icons/Svg/IconStore';
import IconFollow from '../../../Icons/Svg/IconFollow';
import IconCall from '../../../Icons/Svg/IconCall';
import IconLocation from '../../../Icons/Svg/IconLocation';

export default function OptionMenu() {
    return(
        <div className="postoptionsmenu">
            <ul>
                <li className="storedetails">
                    <div className="storelog">
                        <img src="https://static.wixstatic.com/media/abe5aa_708c38ef866e4dd098250b880553b938~mv2.png" className="background"/>
                    </div>
                    <div className="storedata">
                        <h4>Name of store</h4>
                        <span className="storecat">Super market</span>
                        {/* open close  */}
                        <span className="opening open">Open Now : 8 AM to 10 PM</span>
                    </div>
                </li>
                <li>
                    <IconBag/>
                    <span className="menutitle">Shop now</span>
                </li>
                <li>
                    <IconFollow/>
                    <span className="menutitle">Follow this store</span>
                </li>
                <li>
                    <IconCall/>
                    <span className="menutitle">Call now</span>
                </li>
                <li>
                    <IconLocation/>
                    <span className="menutitle">Navigate to this store</span>
                </li>
                <li className="gotostore">
                    <IconButton aria-label="view store">
                        <IconStore /> Go to store page
                    </IconButton>
                </li>
            </ul>
        </div>
    );
}