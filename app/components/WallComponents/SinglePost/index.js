import React, { useEffect, memo } from 'react';
import {
  Avatar,
  Badge,
  IconButton,
  SwipeableDrawer,
  Hidden,
  Menu,
  Button,
  Typography,
} from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import './styles/singlepost.min.css';

import AddToCartButtons from './AddToCartButtons/Loadable';
import LikeCommentIcons from './LikeCommentIcons/Loadable';

import IconCart from '../../Icons/Svg/IconCart';
import IconFollow from '../../Icons/Svg/IconFollow';

import OptionMenuItems from './OptionsMenu/Loadable';

const StyledBadge = withStyles(theme => ({
  badge: {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: '$ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    },
  },
}))(Badge);

export default function SinglePost() {
  const matches = useMediaQuery('(min-width:1024px)');
  // let dropDownOptions;
  // if(matches){
  //     console.log('ok');
  // }else{

  // }
  const [state, setState] = React.useState({
    bottom: false,
  });

  const toggleDrawer = (anchor, open) => event => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className="singlepostwrapper">
      <Hidden only={['sm', 'xs']}>
        <div className="businesslogo">
          <div className="logobuz">
            <img
              src="https://static.wixstatic.com/media/abe5aa_708c38ef866e4dd098250b880553b938~mv2.png"
              className="background"
            />
          </div>
          <Typography variant="h1" component="h3" noWrap>
            Keells Supper store kalutara
          </Typography>
          <div className="buttondata">
            <Button
              variant="contained"
              color="primary"
              startIcon={<IconFollow />}
              className="rounded"
            >
              {' '}
              Follow Business
            </Button>
          </div>
        </div>
      </Hidden>
      <div className="wraprightdata">
        <div className="headerwrap">
          <div className="proimg">
            <Hidden mdUp>
              <StyledBadge
                overlap="circle"
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'right',
                }}
                variant="dot"
              >
                <Avatar
                  alt="Remy Sharp"
                  src="https://material-ui.com/static/images/avatar/1.jpg"
                />
              </StyledBadge>
            </Hidden>
            <Hidden only={['sm', 'xs']}>
              {/* Post related icon */}
              <div className="postrelateicon">
                <IconCart />
              </div>
            </Hidden>
          </div>
          <div className="middledatawrapper">
            <div className="maintitle">
              <a href="">Shop name</a> added new product
            </div>
            <span classNeme="time">10 min ago</span>
          </div>

          <div className="optionbtn">
            <Hidden mdUp>
              <IconButton
                aria-label="options"
                onClick={toggleDrawer('bottom', true)}
              >
                <MoreHorizIcon />
              </IconButton>

              <SwipeableDrawer
                anchor="bottom"
                open={state.bottom}
                onClose={toggleDrawer('bottom', false)}
                onOpen={toggleDrawer('bottom', true)}
                className="singlepostdrawer"
              >
                <OptionMenuItems />
              </SwipeableDrawer>
            </Hidden>

            <Hidden only={['sm', 'xs']}>
              <IconButton
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                onClick={handleClick}
              >
                <MoreHorizIcon />
              </IconButton>
              <Menu
                elevation={0}
                getContentAnchorEl={null}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'right',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
                id="customized-menu"
                className="removepadding"
              >
                <OptionMenuItems />
              </Menu>
            </Hidden>
          </div>
        </div>

        <div className="postmiddledata">
          <div className="sinfullimg">
            <img src="https://static1.squarespace.com/static/5709c194e321403c55496d30/578f13aa414fb579e9fa8656/5a83959b08522971783e3a96/1530054277888/shutterstock_590873999.jpg?format=2500w" />
          </div>
        </div>

        <div className="postbottomdata">
          <div className="contwrap">
            <h3>Essentioal package from keells</h3>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s
            </p>
          </div>
        </div>
        {/* Add to cart buttons for wall post */}
        <AddToCartButtons />

        {/* Like comment icons */}
        <LikeCommentIcons />
      </div>
    </div>
  );
}
