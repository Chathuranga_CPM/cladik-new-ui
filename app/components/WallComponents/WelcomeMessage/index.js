import React from 'react';
import PropTypes from 'prop-types';

import './styles/welcomemessage.min.css';

import WelcomeImg from '../../../images/svg/shopping.svg';
// import emoji from '../../../images/png/smile-face-with-sunglasses.png';

function WelcomeMessage(props) {
  const { user } = props;

  return (
    <div className="welcomemsgwrap">
      <div className="leftimg">
        <img src={WelcomeImg} alt="" />
      </div>
      <div className="rightconte">
        <h1>Start Shopping with Randika</h1>
        {/* <h2>Like a pro <img src={emoji}/></h2> */}
        <p>Lorem Ipsum is simply dummy text of the printing</p>
        <p>This is my first react component</p>
      </div>
    </div>
  );
}

WelcomeMessage.propTypes = {
  user: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }),
};

export default WelcomeMessage;
