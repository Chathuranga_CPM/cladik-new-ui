
import React, { useEffect, memo } from 'react';
import {Link} from 'react-router-dom';
import { Avatar, Badge, Button, SwipeableDrawer } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import './styles/desktopmainmenu.min.css';
import IconHome from '../Icons/Svg/IconHome';
import IconCart from '../Icons/Svg/IconCart';
import IconChat from '../Icons/Svg/IconChat';
import IconNotification from '../Icons/Svg/IconNotification';

import SideProfileCard from '../SideProfileCard/Loadable';

export default function DesktopMainMenu() {
    
    const [state, setState] = React.useState({
        left: false,
    });

    const toggleDrawer = (anchor, open) => (event) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setState({ ...state, [anchor]: open });
    };
    return (
        <div className="desktopmainmenuwrapper">
            
            <div className="innerilne" style={{backgroundColor:'#271da8'}}>
                <div className="mainlogo">
                    <Link to="">
                        <img src="https://cladik.com/assets/img/cladik-logo2.svg" />
                    </Link>
                </div>
                <div className="mainmenulist">
                    <ul>
                        <li>
                            <IconHome/>
                        </li>
                        <li>
                            <Badge badgeContent={4} color="error" overlap="circle" >
                                <IconCart/>
                            </Badge>
                        </li>
                        <li>
                            <IconChat/>
                        </li>
                        <li>
                            <IconNotification/>
                        </li>
                    </ul>
                </div>
                <div className="userimg">
                    <Button>
                        <Badge badgeContent={4} color="error" overlap="circle" onClick={toggleDrawer('left', true)}>
                            <Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" />
                        </Badge>
                    </Button>
                    <SwipeableDrawer
                        anchor='left'
                        open={state['left']}
                        onClose={toggleDrawer('left', false)}
                        onOpen={toggleDrawer('left', true)}
                        className="mainheadersidedrawer"
                    >
                        <div className="wrapdrawerinner">
                            <SideProfileCard/>
                            {/* <h2>Cart</h2> */}
                        </div>
                    </SwipeableDrawer>
                </div>
            </div>
        </div>
    );
}