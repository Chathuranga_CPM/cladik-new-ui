import React, { useEffect, memo } from 'react';
import { withWidth, Typography, IconButton, Menu } from '@material-ui/core';
import { Link } from 'react-router-dom';

import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import IconCart from '../Icons/Svg/IconCart';

import './styles/latestshopwall.min.css';

export default function LatestShopWall() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div className="latestshopwrapper">
      <div className="headtitle">
        <div className="titlearea">
          <h2>Other Stores</h2>
          <span>Recommonded store</span>
        </div>
        <IconButton
          aria-controls="customized-menu"
          aria-haspopup="true"
          variant="contained"
          onClick={handleClick}
        >
          <MoreHorizIcon />
        </IconButton>
        <Menu
          elevation={0}
          getContentAnchorEl={null}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          id="customized-menu"
          className="removepadding"
        >
          drop data
        </Menu>
      </div>

      <div className="sinstorewrapper">
        <Link to="/">
          <div className="bannerimg">
            <img
              src="https://cdn.britannica.com/16/204716-050-8BB76BE8/Walmart-store-Mountain-View-California.jpg"
              alt=""
              className="background"
            />
          </div>
          <div className="content">
            <h3 className="nowrap">Wallmart supper market</h3>
            <p>
              Discount store, in merchandising, a retail store that sells
              products at prices lower than those asked by traditional retail
              outlets.
            </p>
          </div>
        </Link>
      </div>

      <div className="moreviewlink">
        <Link to="/">View all stores</Link>
      </div>
    </div>
  );
}
