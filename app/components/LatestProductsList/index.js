import React, { useEffect, memo } from 'react';
import {withWidth, Typography, IconButton, Menu} from '@material-ui/core';
import { Link } from 'react-router-dom';

import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import IconCart from '../Icons/Svg/IconCart';


import './styles/latestproductslist.min.css';


export default function LatestProductsList() {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };
    return (
        <div className="latestprowrapper">
            <div className="singlecardright">
                <div className="headtitle">
                    <div className="titlearea">
                        <h2>Latest Products</h2>
                        <span>List of all the latest products</span>
                    </div>
                    <IconButton aria-controls="customized-menu" aria-haspopup="true" variant="contained" onClick={handleClick}>
                        <MoreHorizIcon />
                    </IconButton>
                    <Menu
                        elevation={0}
                        getContentAnchorEl={null}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                        id="customized-menu"
                        className="removepadding"
                    >
                        drop data
								</Menu>
                </div>
                <ul>
                    <li>
                        <div className="prodimg">
                            <img className="background" src="https://i.insider.com/5d0bc2a0e3ecba03841d82d2?width=960&format=jpeg" alt=""/>
                        </div>
                        <div className="prodconte">
                            <Link to="/">
                                <Typography variant="span" component="span" noWrap>
                                    Product title goes here
                                </Typography>
                            </Link>
                            <Link to="/cat" className="cat">Category name</Link>
                            <div className="price">LKR 1500.00/=</div>
                        </div>
                        <div className="carticon">
                        <IconButton aria-label="add-to-cart">
                            <IconCart/>
                        </IconButton>
                        </div>
					</li>
                    <li>
                        <div className="prodimg">
                            <img className="background" src="https://i.insider.com/5d0bc2a0e3ecba03841d82d2?width=960&format=jpeg" alt=""/>
                        </div>
                        <div className="prodconte">
                            <Link to="/">
                                <Typography variant="span" component="span" noWrap>
                                    Product title goes here
                                </Typography>
                            </Link>
                            <Link to="/cat" className="cat">Category name</Link>
                            <div className="price">LKR 1500.00/=</div>
                        </div>
                        <div className="carticon">
                        <IconButton aria-label="add-to-cart">
                            <IconCart/>
                        </IconButton>
                        </div>
					</li>
                    <li>
                        <div className="prodimg">
                            <img className="background" src="https://i.insider.com/5d0bc2a0e3ecba03841d82d2?width=960&format=jpeg" alt=""/>
                        </div>
                        <div className="prodconte">
                            <Link to="/">
                                <Typography variant="span" component="span" noWrap>
                                    Product title goes here
                                </Typography>
                            </Link>
                            <Link to="/cat" className="cat">Category name</Link>
                            <div className="price">LKR 1500.00/=</div>
                        </div>
                        <div className="carticon">
                        <IconButton aria-label="add-to-cart">
                            <IconCart/>
                        </IconButton>
                        </div>
					</li>
                </ul>
                <div className="moreviewlink">
                    <Link to='/'>View all</Link>
                </div>
            </div>
        </div>
    );
}