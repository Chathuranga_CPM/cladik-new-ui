import React from 'react';
import { Link } from 'react-router-dom';

import { makeStyles, fade, createMuiTheme } from '@material-ui/core/styles';

import AddToCartButtons from '../../WallComponents/SinglePost/AddToCartButtons/Loadable';

import './styles/singleproductcard.min.css';

const theme = createMuiTheme();

const useStyles = makeStyles(theme => ({
  setthemecolor: {
    color: `${fade(theme.palette.primary.main, 1)}`,
  },
}));

export default function SingleProductCard() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
  });
  return (
    <div className="sinprodwrap">
      <div className="topimgwrapper">
        <Link to="/single-product">
          <div className="productimg">
            <img
              src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/shopping-bag-full-of-fresh-vegetables-and-fruits-royalty-free-image-1128687123-1564523576.jpg"
              alt=""
              className="background"
            />
          </div>
        </Link>
      </div>

      <div className="bottomprodata">
        <h3 className="nowrap">This is a product name</h3>
        <div className="price">
          <span className={`${classes.setthemecolor} reguler`}>$99.00</span>
          <span className="sales">
            <del>$10.00</del>
          </span>
        </div>
        <div className="addtocartbtns">
          <AddToCartButtons />
        </div>
      </div>
    </div>
  );
}
