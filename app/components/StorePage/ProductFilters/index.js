import React from 'react';
import { IconButton, Hidden, SwipeableDrawer } from '@material-ui/core';
import FilterListIcon from '@material-ui/icons/FilterList';
import FilterItems from './FilterItems';

import './styles/productfilters.min.css';

export default function ProductFilters() {
  const [state, setState] = React.useState({
    top: false,
  });

  const toggleDrawer = (anchor, open) => event => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };
  return (
    <div className="filterlistwrap">
      <div className="headertitle">
        <h3>Products filters</h3>
        <Hidden mdUp>
          <IconButton aria-label="expand" onClick={toggleDrawer('top', true)}>
            <FilterListIcon />
          </IconButton>
        </Hidden>
      </div>

      <Hidden mdUp>
        <SwipeableDrawer
          anchor="top"
          open={state.top}
          onClose={toggleDrawer('top', false)}
          onOpen={toggleDrawer('top', true)}
          className="productfilterspopup"
        >
          <FilterItems />
        </SwipeableDrawer>
      </Hidden>
    </div>
  );
}
