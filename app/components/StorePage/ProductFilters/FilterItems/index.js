import React from 'react';
import PropTypes from 'prop-types';
import SvgIcon from '@material-ui/core/SvgIcon';
import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import Collapse from '@material-ui/core/Collapse';
import { fade, makeStyles, withStyles } from '@material-ui/core/styles';

import { useSpring, animated } from 'react-spring/web.cjs';
import ClearInput from '../../../ClearInput';

import IconSearch from '../../../Icons/Svg/IconSearch';

import FolderIcon from '@material-ui/icons/Folder';
import FolderOpenIcon from '@material-ui/icons/FolderOpen';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import '../styles/productfilters.min.css';

function MinusSquare(props) {
    return (
        <FolderOpenIcon />
    );
}

function PlusSquare(props) {
    return (
        <FolderIcon />
    );
}

function CloseSquare(props) {
    return (
        <NotInterestedIcon />
    );
}

function TransitionComponent(props) {
    const style = useSpring({
        from: { opacity: 0, transform: 'translate3d(20px,0,0)' },
        to: { opacity: props.in ? 1 : 0, transform: `translate3d(${props.in ? 0 : 20}px,0,0)` },
    });

    return (
        <animated.div style={style}>
            <Collapse {...props} />
        </animated.div>
    );
}

TransitionComponent.propTypes = {
    /**
     * Show the component; triggers the enter or exit states
     */
    in: PropTypes.bool,
};


const StyledTreeItem = withStyles((theme) => ({
    iconContainer: {
        '& .close': {
            opacity: 0.3,
        },
    },
    group: {
        marginLeft: 7,
        paddingLeft: 18,
        borderLeft: `1px dashed ${fade(theme.palette.text.primary, 0.4)}`,
    },
}))((props) => <TreeItem {...props} TransitionComponent={TransitionComponent} />);

const useStyles = makeStyles({
    root: {
        height: 'auto',
        flexGrow: 1,
        maxWidth: 400,
    },
});

export default function FilterItems() {
    const classes = useStyles();
    return (
        <div className="filterlistwrap">
            <div className="filterslistwrap">
                <div className="seachwrap">
                    <IconSearch />
                    <ClearInput placeholder="Search products" />
                </div>

                <div className="procatlist">
                    <h4>Filter by categories</h4>
                    <div className="listofcat">
                        <TreeView
                            className={classes.root}
                            defaultExpanded={['1']}
                            defaultCollapseIcon={<MinusSquare />}
                            defaultExpandIcon={<PlusSquare />}
                            defaultEndIcon={<CloseSquare />}
                        >
                            <StyledTreeItem nodeId="1" label="Main" />
                            <StyledTreeItem nodeId="2" label="Hello" />
                            <StyledTreeItem nodeId="3" label="Subtree with children">
                                <StyledTreeItem nodeId="6" label="Hello" />
                                <StyledTreeItem nodeId="7" label="Sub-subtree with children">
                                    <StyledTreeItem nodeId="9" label="Child 1" />
                                    <StyledTreeItem nodeId="10" label="Child 2" />
                                    <StyledTreeItem nodeId="11" label="Child 3" />
                                </StyledTreeItem>
                                <StyledTreeItem nodeId="8" label="Hello" />
                            </StyledTreeItem>
                            <StyledTreeItem nodeId="4" label="World" />
                            <StyledTreeItem nodeId="5" label="Something something" />
                        </TreeView>
                    </div>
                </div>
            </div>
        </div>

    );
}