import React from 'react';
import {Hidden, IconButton} from '@material-ui/core';
import { Link } from 'react-router-dom';

import IconLink from '../../../Icons/Svg/IconLink';
import IconFacebook from '../../../Icons/Svg/IconFacebook';
import IconWhatsapp from '../../../Icons/Svg/IconWhatsapp';
import IconLinkedin from '../../../Icons/Svg/IconLinkend';
import IconTwitter from '../../../Icons/Svg/IconTwitter';
import IconEmail from '../../../Icons/Svg/IconEmail';
import IconSMS from '../../../Icons/Svg/IconSMS';

import './styles/sharesheet.min.css'

import { browserName, CustomView, osName } from 'react-device-detect';


export default function ShareSheet() {
    return(
        <div className="sharingoptionssheet">
            <h2>Share this page</h2>
            <div className="toprow">
                <ul>
                    <li>
                        <div className="copylink">
                            <IconLink/>
                            <span>Copy link</span>
                        </div>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.sharelinkgenerator.com/" target="_blank" className="fb">
                            <IconFacebook/>
                            <span>Facebook</span>
                        </a>
                    </li>
                    <li>
                        <a href="https://api.whatsapp.com/send?text=Hey i found cool website for improve your business online check this out - https://www.sharelinkgenerator.com/" target="_blank" className="wta">
                            <IconWhatsapp/>
                            <span>Whatsapp</span>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://www.sharelinkgenerator.com/&title=Hey i found cool website for improve your business online check this out&summary=&source=" target="_blank" className="lin">
                            <IconLinkedin/>
                            <span>Linkedin</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://twitter.com/share?text=Hey i found cool website for improve your business online check this out&url=https://www.sharelinkgenerator.com/&hashtags=businessnetwork,cladik,improvebusiness" target="_blank" className="twt">
                            <IconTwitter/>
                            <span>Twitter</span>
                        </a>
                    </li>
                    <li>
                        <a href="mailto:?&subject=&body=Hey i found cool website for improve your business online check this out - https://www.sharelinkgenerator.com/" target="_blank" className="email">
                            <IconEmail/>
                            <span>Email</span>
                        </a>
                    </li>
                    <CustomView condition={osName === "iOS" || osName === "Android"}>    
                    <li>
                        <a href="sms:&body=Hey i found cool website for improve your business online check this out - https://www.sharelinkgenerator.com/" target="_blank" className="sms">
                            <IconSMS/>
                            <span>Message (SMS)</span>
                        </a>
                    </li>
                    </CustomView>

                </ul>
            </div>
        </div>
    );
}