import React from 'react';
import { Hidden, IconButton, SwipeableDrawer, Menu } from '@material-ui/core';
import { Link } from 'react-router-dom';

import { makeStyles, fade, createMuiTheme } from '@material-ui/core/styles';

import ShareSheet from './ShareSheet/Loadable';

import './styles/storepageheader.min.css';

import IconLocation from '../../Icons/Svg/IconLocation';
import IconCall from '../../Icons/Svg/IconCall';
import IconFollow from '../../Icons/Svg/IconFollow';
import IconShare from '../../Icons/Svg/IconShare';

// Theming
const theme = createMuiTheme();

console.log(theme.palette.type);

const useStyles = makeStyles(theme => ({
  catename: {
    background: `${fade(theme.palette.primary.main, 0.1)}`,
    color: `${fade(theme.palette.primary.main, 1)}`,
    '&:hover': {
      background: `${fade(theme.palette.primary.main, 1)}`,
      color: '#fff',
    },
  },
  settheme: {
    backgroundColor: `${fade(theme.palette.primary.main, 0.05)}`,
    '&:focus, &:active': {
      backgroundColor: `${fade(theme.palette.primary.main, 0.05)}`,
    },
    '&:hover': {
      backgroundColor: `${fade(theme.palette.primary.main, 0.1)}`,
    },
  },
}));

export default function StorePageHeader() {
  const classes = useStyles();

  const [state, setState] = React.useState({
    bottom: false,
  });

  const toggleDrawer = (anchor, open) => event => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className="storeheaderwrapper">
      <Hidden only={['lg', 'md', 'xl']}>
        <div className="mobheightset" />
      </Hidden>
      <div className="coverimgwrapper">
        <img
          src="https://media-assets-04.thedrum.com/cache/images/thedrum-prod/s3-news-tmp-123315-keells--default--1280.png"
          alt="store name should be here"
          className="background"
        />
      </div>

      <div className="optionheaderwrap">
        <div className="logoandcalltitle">
          <div className="logo">
            <img
              src="https://static.wixstatic.com/media/abe5aa_708c38ef866e4dd098250b880553b938~mv2.png"
              alt=""
              className="background"
            />
          </div>
          <div className="titlearea">
            <h1>Keells super market</h1>
            <div className="follows">
              <IconFollow />
              <span>15k Follow</span>
            </div>
            <Link to="/cat" className={classes.catename}>
              Category name
            </Link>
          </div>
        </div>
        <div className="calltoactionwrap">
          <ul>
            <li>
              <IconButton
                aria-label="direction"
                onClick=""
                className={classes.settheme}
              >
                <IconLocation />
              </IconButton>
            </li>
            <li>
              <IconButton
                aria-label="follow"
                onClick=""
                className={classes.settheme}
              >
                <IconFollow />
              </IconButton>
            </li>
            <li>
              <IconButton
                aria-label="call"
                onClick=""
                className={classes.settheme}
              >
                <IconCall />
              </IconButton>
            </li>

            <li>
              <Hidden mdUp>
                <IconButton
                  aria-label="share"
                  onClick={toggleDrawer('bottom', true)}
                  className={classes.settheme}
                >
                  <IconShare />
                </IconButton>
              </Hidden>
              <Hidden only={['sm', 'xs']}>
                <IconButton
                  aria-controls="share"
                  aria-haspopup="true"
                  variant="contained"
                  className={classes.settheme}
                  onClick={handleClick}
                >
                  <IconShare />
                </IconButton>
                <Menu
                  elevation={0}
                  getContentAnchorEl={null}
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  anchorEl={anchorEl}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                  id="customized-menu"
                  className="removepadding borderset"
                  disableScrollLock
                >
                  <ShareSheet />
                </Menu>
              </Hidden>
            </li>
          </ul>
        </div>
      </div>

      {/* Share sheet */}
      <SwipeableDrawer
        anchor="bottom"
        open={state.bottom}
        onClose={toggleDrawer('bottom', false)}
        onOpen={toggleDrawer('bottom', true)}
        className="sharesheetpopup"
      >
        <ShareSheet />
      </SwipeableDrawer>
    </div>
  );
}
