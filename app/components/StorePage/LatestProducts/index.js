import React from 'react';
import { Link } from 'react-router-dom';
import {
  IconButton,
  TextField,
  SwipeableDrawer,
  Grid,
  Paper,
} from '@material-ui/core';
import FilterListIcon from '@material-ui/icons/FilterList';
import { makeStyles, fade, createMuiTheme } from '@material-ui/core/styles';
import SingleProductCard from '../SingleProductCard/Loadable';

import './styles/latestproducts.min.css';

// Theming
const theme = createMuiTheme();

const useStyles = makeStyles(theme => ({
  setthemecolor: {
    color: `${fade(theme.palette.primary.main, 1)}`,
  },
}));
export default function LatestProducts() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
  });

  const toggleDrawer = (anchor, open) => event => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };
  return (
    <div className="latestproductswrapper">
      <div className="headertitle">
        <h3>Latest Products</h3>
        <Link to="/" className={classes.setthemecolor}>
          View All
        </Link>
      </div>

      <div className="gridviewproducts">
        <div className="innerscrollerarea">
          <div className="sincard">
            <SingleProductCard />
          </div>
          <div className="sincard">
            <SingleProductCard />
          </div>
          <div className="sincard">
            <SingleProductCard />
          </div>
          <div className="sincard">
            <SingleProductCard />
          </div>
        </div>
      </div>
    </div>
  );
}
