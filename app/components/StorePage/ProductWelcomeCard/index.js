import React from 'react';
import {Button} from '@material-ui/core';

import IconCart from '../../Icons/Svg/IconCart';

import './styles/productwelcomecard.min.css';

// Theming
import { makeStyles, fade, createMuiTheme } from '@material-ui/core/styles';
const theme = createMuiTheme();
const useStyles = makeStyles((theme) => ({
    setthemecolor:{
        backgroundColor:`${fade(theme.palette.primary.main, 0.1)}`,
    }
}));
  

export default function ProductWelcomeCard() {
    const classes = useStyles();
    return(
        <div className={classes.setthemecolor+' productwelcomecardwrap'}>
            <div className="leftcard">
                <img src="https://via.placeholder.com/300x200"/>
            </div>
            <div className="rightconte">
                <h1 >Essentioal package </h1>
                <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                </p>
                <div className="btnwrap clearfix">
                    <Button
                        variant="contained"
                        color="primary"
                        className="rounded"
                        endIcon={<IconCart/>}
                    >
                        Add to bucket
                    </Button>
                </div>
            </div>
        </div>
    )
}