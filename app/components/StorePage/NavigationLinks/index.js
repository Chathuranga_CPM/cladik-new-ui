import React, { useEffect, memo } from 'react';
import { Avatar, Badge } from '@material-ui/core';
import { Link } from 'react-router-dom';
import IconHome from '../../Icons/Svg/IconHome';
import IconPost from '../../Icons/Svg/IconPost';
import IconCart from '../../Icons/Svg/IconCart';
import IconProfile from '../../Icons/Svg/IconProfile';
import IconMessenger from '../../Icons/Svg/IconMessenger';

import './styles/storenavigation.min.css';

export default function NavigationLinks() {
  return (
    <div className="storenavi">
      <div className="scrollerdata">
        <ul>
          <li className="active">
            <Link to="/">
              <IconHome />
              <span>Home</span>
            </Link>
          </li>
          <li>
            <Link to="/">
              <IconPost />
              <span>Post</span>
            </Link>
          </li>
          <li>
            <Link to="/">
              <IconCart />
              <span>Products</span>
            </Link>
          </li>
          <li>
            <Link to="/">
              <IconPost />
              <span>Sevices</span>
            </Link>
          </li>
          <li>
            <Link to="/">
              <IconProfile />
              <span>About</span>
            </Link>
          </li>
          <li>
            <Link to="/">
              <IconMessenger />
              <span>Contact</span>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
}
