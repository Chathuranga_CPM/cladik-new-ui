import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';

import IconStore from '../Icons/Svg/IconStore';
import IconCall from '../Icons/Svg/IconCall';

import './styles/smallsellercard.min.css';

export default function SmallSellerCard() {
  return (
    <div className="smallsellercardwrapper">
      <div className="headtitle">
        <span className="nowrap">Keells Supper Maharagama</span>
      </div>
      <div className="storedata">
        <div className="icon">
          <img
            src="https://static.wixstatic.com/media/abe5aa_708c38ef866e4dd098250b880553b938~mv2.png"
            className="background"
          />
        </div>
        <div className="calltoactions">
          <ul>
            <li>
              <Button
                variant="contained"
                color="default"
                startIcon={<IconStore />}
              >
                Go to Store
              </Button>
            </li>
            <li>
              <Button
                variant="contained"
                color="default"
                startIcon={<IconCall />}
              >
                Contact
              </Button>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
