
import React, { useEffect, memo } from 'react';
import {
    BottomNavigation,
    BottomNavigationAction
} from '@material-ui/core';
import IconHome from '../Icons/Svg/IconHome';
import IconCart from '../Icons/Svg/IconCart';
import IconChat from '../Icons/Svg/IconChat';
import IconNotification from '../Icons/Svg/IconNotification';

// This is a store theme color
import { createMuiTheme, fade, makeStyles } from '@material-ui/core/styles';

const theme = createMuiTheme();


import './styles/mobilebottomnav.min.css';

const useStyles = makeStyles((theme) => ({
    root:{
        background:`${fade(theme.palette.primary.main, 1)}`,
        borderRadius: 30,
    }
}));



export default function MobileBottomNavigation() {
    const classes = useStyles();
    const [value, setValue] = React.useState('home');

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    return (
        <div className="mobilebottomnav">
            <BottomNavigation value={value} onChange={handleChange} className={classes.root}>
                <BottomNavigationAction  value="home" icon={<IconHome />} />
                <BottomNavigationAction value="cart" icon={<IconCart />} />
                <BottomNavigationAction value="chat" icon={<IconChat />} />
                <BottomNavigationAction value="notification" icon={<IconNotification />} />
            </BottomNavigation>
        </div>
    );
}