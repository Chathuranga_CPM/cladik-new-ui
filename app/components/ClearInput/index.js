import React from 'react';
import { InputBase } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
    withStyles,
} from '@material-ui/core/styles';


const CustomInput = withStyles((theme) => ({
    root: {
        'label + &': {
            marginTop: theme.spacing(3),
        },
    },
    input: {
        borderRadius: 4,
        position: 'relative',
        border: '1px solid #ced4da',
        fontSize: 16,
        width: 'auto',
        padding: '10px 12px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        
        '&:focus': {
            boxShadow: `0 0 0`,
            borderColor: 'none',
        },
    },
}))(InputBase);

export default function ClearInput(props) {
    return (
        <CustomInput fullWidth id="clear-input" placeholder={props.placeholder}/>
    )
}