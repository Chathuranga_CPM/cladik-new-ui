
import React, { useEffect, memo } from 'react';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import './styles/addproductbutton.min.css';


export default function AddProductFabButton() {
    
    return (
        <div className="floatbutton">
            <Fab color="primary" aria-label="add">
                <AddIcon />
            </Fab>
        </div>
    );
}